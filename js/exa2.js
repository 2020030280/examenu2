function cambiar(Origen){
    let MD= document.getElementById('MDestino');
    
    if(Origen.value=="1"){
        MD.options[1]=new Option('Dolar Estadounidense','1');
        MD.options[2]=new Option('Dolar Canadiense','2');
        MD.options[3]=new Option('Euro','3');
    }else if(Origen.value=="2"){
        MD.options[1]=new Option('Peso Mexicano','1');
        MD.options[2]=new Option('Dolar Canadiense','2');
        MD.options[3]=new Option('Euro','3');
    }else if(Origen.value=="3"){
        MD.options[1]=new Option('Peso Mexicano','1');
        MD.options[2]=new Option('Dolar Estadounidense','2');
        MD.options[3]=new Option('Euro','3');
    }else if(Origen.value=="4"){
        MD.options[1]=new Option('Peso Mexicano','1');
        MD.options[2]=new Option('Dolar Estadounidense','2');
        MD.options[3]=new Option('Dolar Canadiense','3');
    }
    }
    function calcular(){
        let cantidad= document.getElementById('can').value;
        let MD= document.getElementById('MDestino');
        let Origen=document.getElementById('MOrigen');
        let subtotal=0;
        let comision=0;
        let total=0;
    
        if(Origen.value=="1"){
            switch(MD.value){
                case "1":
                    subtotal=(cantidad/19.85).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "2":
                    subtotal=((cantidad/19.85)*1.35).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "3":
                subtotal=((cantidad/19.85)*0.99).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;    
                default:
                    break;    
            }
        }else if(Origen.value=="2"){
            switch(MD.value){
                case "1":
                    subtotal=(cantidad*19.85).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "2":
                    subtotal=(cantidad*1.35).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "3":
                subtotal=(cantidad*0.99).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;    
                default:
                    break;    
            }
        }else if(Origen.value=="3"){
            switch(MD.value){
                case "1":
                    subtotal=((cantidad*19.85)/1.35).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "2":
                    subtotal=((cantidad/1.35)).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "3":
                subtotal=((cantidad*0.99)/1.35).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;    
                default:
                    break;    
            }
        }else if(Origen.value=="4"){
            switch(MD.value){
                case "1":
                    subtotal=((cantidad*19.85)/0.99).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "2":
                    subtotal=(cantidad/0.99).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "3":
                subtotal=((cantidad*1.35)/0.99).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;    
                default:
                    break;    
            }
        }
        document.getElementById('sub').value=subtotal;
        document.getElementById('comi').value=comision;
        document.getElementById('tot').value=total;
    
    
    }
    var acumulado=0;
function registrar(){
    let acumular=document.getElementById('tot').value;
    let parrafo=document.getElementById('parrafo');
    let totg= document.getElementById('general');
    
    let parrafo2;
    var seleccionada = document.getElementById('MOrigen')[document.getElementById('MOrigen').selectedIndex].text;
    var seleccionada2 = document.getElementById('MDestino')[document.getElementById('MDestino').selectedIndex].text;
    let subtotal=document.getElementById('sub').value;
    let comision=document.getElementById('comi').value;
    let total=document.getElementById('tot').value;
    let cantidad= document.getElementById('can').value;
    parrafo2=cantidad + " " + seleccionada + " a " + seleccionada2 + " " + subtotal + " "
    + comision + " " + total + "<br>";
    parrafo.innerHTML= parrafo.innerHTML + parrafo2;

    acumulado=  parseFloat(acumular) + acumulado ;
    totg.innerText=acumulado;
}
function borrar(){
    let parrafo=document.getElementById('parrafo');
    let totg= document.getElementById('general');
    totg.innerHTML="";
    parrafo.innerHTML="<br><br>";
}